package utcn.piu.android_2.model;

/**
 * Created by Student on 11/20/2019.
 */

public class Offer {
    private String title;
    private Integer image;
    private String description;
    private String price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Offer(String title, Integer image, String description, String price) {
        this.title = title;
        this.image = image;
        this.description = description;
        this.price = price;
    }

    public Offer() {

    }
}
