package utcn.piu.android_2;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import utcn.piu.android_2.adapters.myAdapter;
import utcn.piu.android_2.model.Offer;
import utcn.piu.android_2.service.OfferService;

public class OfferActivity extends AppCompatActivity {


    private ListView listOffer;
    private myAdapter adapter;
    private List<Offer> oferte;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        listOffer = findViewById(R.id.offer_list);
        oferte = OfferService.getOffers();

        adapter = new myAdapter ( this, OfferService.getOffers());
        //	aici	se	va	trimite	și	lista	de	obiecte
        listOffer.setAdapter(adapter);
        registerForContextMenu(listOffer);
    }

    public	void	onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu,	v, menuInfo);
        //	verificăm	dacă	meniul	este	creat	pentru	lista	vizată
        if	(v.getId()== R.id.offer_list) {
            //	identificăm	elementul	selectat	din	listă
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(adapter.getItem(info.position).getTitle());
            //	încărcăm structura vizuală a meniului
            getMenuInflater().inflate(R.menu.myenu,	menu);
        }
    }



    public	boolean	onCreateOptionsMenu(Menu menu)
    {
        //	Inflate the	menu;	this	adds	items	to	the	action	bar	if	it	is	present.
        getMenuInflater().inflate(R.menu.top_right_menu,	menu);
        return	true;
    }

   public void signOut () {
       final AlertDialog.Builder myDialog =	new	AlertDialog.Builder(this);
       myDialog
               .setTitle("Sign out")
               .setMessage("Confirmation")
               .setPositiveButton("Confrim", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       finish();
                   }
               })
               .setNegativeButton("Cancel",	new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                        return;
                   }

               })
               .show();
   }

    public	boolean	onOptionsItemSelected(MenuItem	item)
    {
        //	Handle	action	bar	item	clicks	here.	The	action	bar	will
        //	automatically	handle	clicks	on	the	Home/Up	button,	so	long
        //	as	you	specify	a	parent	activity	in	AndroidManifest.xml.
        int	id	=	item.getItemId();
            if	(id	==	R.id.SignOut)
            {
                signOut();
                return	true;
            }
            else	if(id	==	R.id.ResetList)
            {
                oferte = OfferService.getOffers();
                adapter = new myAdapter ( this, OfferService.getOffers());
                listOffer.setAdapter(adapter);
                registerForContextMenu(listOffer);

                Toast.makeText(this,"The list has been reset.", Toast.LENGTH_LONG).show();

                return	true;
            }	else	if (id	==	R.id.ClearFav) {
                Toast.makeText(this,"Favourites have been reset", Toast.LENGTH_LONG).show();

            } else if (id == R.id.ChatwithUs){
                Intent intent = new Intent(this, Chat_Activity.class);
                startActivity(intent);

            }
        return	super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        if(item.getItemId() == R.id.add) {
            Offer offer1 = new Offer("Easter Island", R.drawable.offer_1,"Luxury holiday","100000EUR");
            oferte.add(info.position, offer1);

        }
        if(item.getItemId() == R.id.remove) {
            oferte.remove(info.position);

        }

        adapter = new myAdapter ( this, oferte);
        //	aici	se	va	trimite	și	lista	de	obiecte
        listOffer.setAdapter(adapter);
        registerForContextMenu(listOffer);

        return super.onContextItemSelected(item);
    }


    /*Button signOutBtn = findViewById(R.id.signOutBtn);
    signOutBtn.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v){

            Intent intent = new Intent(MainActivity.this, OfferActivity.class);
            startActivity(intent);
        }
    });*/

}
