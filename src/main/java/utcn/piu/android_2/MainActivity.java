package utcn.piu.android_2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity
{

    String userCheck;
    String passCheck;


    private EditText username;
    private EditText password;
    private TextView errUser;
    private TextView errPass;
    private TextView loginSuccess;
    private TextView loginFailed;

    public MainActivity()
    {
        this.userCheck = "admin";
        this.passCheck = "password";
    }


    protected void onCreate(Bundle savedInstanceState)//"starting point of activity" <=> "public static void main()" and the first screen so far of current app
    {
        super.onCreate(savedInstanceState);//callback function from parent class
        setContentView(R.layout.activity_main);//refers to activity_main.xml, method from Activity class
        getXMLAttributeIds();
    }



    public void getXMLAttributeIds()
    {
        username = findViewById(R.id.username);//findVIewById method from Activity class(return type View?)
        password = findViewById(R.id.password);
        errUser = findViewById(R.id.errorusername);
        errPass = findViewById(R.id.errorpassword);
        loginSuccess = findViewById(R.id.successOrFail);
        loginFailed = findViewById(R.id.justFail);
    }


    public void signIn(View forButton)//View parametere needed for the button tag defined in xml to work when clicked
    {
        int lenUsr = username.getText().length();
        int lenPass = password.getText().length();
        int incorrectUserOrPass = 0;


        if (lenUsr == 0)
        {
            errUser.setText("Username cannot be empty!");
        }
        else if (lenUsr < 3)
        {
            errUser.setText("Username is too short!");
        }
        else
        {
            errUser.setText("");
            incorrectUserOrPass++;
        }


        if (lenPass == 0)
        {
            errPass.setText("Password cannot be empty!");
        }
        else if (lenPass < 6)
        {
            errPass.setText("Password is too short!");
        }
        else
        {
            errPass.setText("");
            incorrectUserOrPass++;
        }


        if (userCheck.equals(username.getText().toString()) && passCheck.equals(password.getText().toString()))
        {
            loginSuccess.setTextColor(Color.GREEN);
            loginSuccess.setText("Login successful!");
            loginFailed.setText("");
            Intent intent = new Intent(this, OfferActivity.class);
            startActivity(intent);
        }
        else if (incorrectUserOrPass == 2)
        {
            loginSuccess.setTextColor(Color.RED);
            loginSuccess.setText("Login failed.");
            loginFailed.setTextColor(Color.RED);
            loginFailed.setText("Username or password is incorrect!");
        }
    }
}