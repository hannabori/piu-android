package utcn.piu.android_2.adapters;

import android.content.Context;
import android.gesture.GestureOverlayView;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import utcn.piu.android_2.R;
import utcn.piu.android_2.model.Offer;

/**
 * Created by Student on 11/20/2019.
 */

public class myAdapter extends ArrayAdapter<Offer> {

    public myAdapter(@NonNull Context context, @NonNull List<Offer> objects){
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //	get	a	reference	to	the	LayoutInflater	service
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //	check	if	we	can	reuse	a	previously	defined	cell	which	now	is	not	visible	anymore
        View myRow = (convertView == null) ? inflater.inflate(R.layout.list_element, parent, false)	: convertView;
        //	get	the	visual	elements	and	update	them	with	the	information	from	the	model
        TextView title = myRow.findViewById(R.id.title);
        title.setText(getItem(position).getTitle());

        ImageView image = myRow.findViewById(R.id.image);
        image.setImageResource(getItem(position).getImage());

        TextView description = myRow.findViewById(R.id.description);
        description.setText(getItem(position).getDescription());

        TextView price = myRow.findViewById(R.id.price);
        price.setText(getItem(position).getPrice());

        return	myRow;

    }
}
