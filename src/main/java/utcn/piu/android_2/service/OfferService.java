package utcn.piu.android_2.service;

import java.util.ArrayList;
import java.util.List;

import utcn.piu.android_2.R;
import utcn.piu.android_2.model.Offer;

/**
 * Created by Student on 11/20/2019.
 */

public class OfferService {
    public static List<Offer> getOffers() {
        List<Offer> ourList = new ArrayList<>();
        Offer offer1 = new Offer("Barcelona, 3 nights", R.drawable.offer_1,"Barcelona has many venues...","300EUR");
        ourList.add(offer1);
        Offer offer2 = new Offer("Maldive, 7 nights", R.drawable.offer_2,"Amazing Maldive","1050EUR");
        ourList.add(offer2);
        Offer offer3 = new Offer("Thailand, 10 nights", R.drawable.offer_3,"Luxury in Asia","1455EUR");
        ourList.add(offer3);
        return ourList;
    }
}
